package com.featherlike.feather.generator;

import java.io.File;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.featherlike.feather.generator.config.ConfigProperties;
import com.featherlike.feather.generator.config.Constant;
import com.featherlike.framework.common.util.DateUtil;
import com.featherlike.framework.common.util.MapUtil;
import com.featherlike.framework.common.util.StringUtil;

public class GeneratorCfgImpl implements IGeneratorCfg {
	private static Logger logger = LoggerFactory.getLogger(GeneratorCfgImpl.class);
	// 文件分隔符
	public String FILE_SEPARATOR = File.separator;
	private static GeneratorCfgImpl instance;
	private String packageName;
	private String moduleName;
	private String subModuleName;
	private String classAuthor;
	private String functionName;
	private String projectPath;
	private String tplPath;
	private String excelPath;
	// Java文件路径
	private String sqlPath;
	private String javaPath;
	private String viewPath;

	private GeneratorCfgImpl() {
		init();
	}

	public static GeneratorCfgImpl instance() {
		if (null == instance) {
			instance = new GeneratorCfgImpl();
		}
		return instance;
	}

	public void init() {
		packageName = ConfigProperties.getPackageName();
		moduleName = ConfigProperties.getModuleName();
		subModuleName = ConfigProperties.getSubModule();
		functionName = ConfigProperties.getFuncName();
		classAuthor = ConfigProperties.getAuthorName();

		projectPath = System.getProperty("user.dir");
		logger.info("Project Path: {}", projectPath);
		// Excel path
		excelPath = StringUtil.replace(projectPath + FILE_SEPARATOR
				+ ConfigProperties.getExcelPath(), "/", FILE_SEPARATOR);
		logger.info("Excel Path: {}", excelPath);
		// 模板文件路径
		tplPath = StringUtil.replace(projectPath + FILE_SEPARATOR
				+ ConfigProperties.getTemplateDir() + FILE_SEPARATOR
				+ ConfigProperties.getTemlateEngine(), "/", FILE_SEPARATOR);
		logger.info("Template Path: {}", tplPath);
		javaPath = StringUtil.replaceEach(projectPath + FILE_SEPARATOR
				+ ConfigProperties.getOutputDir() + "/src/main/java/"
				+ StringUtil.lowerCase(packageName), new String[] { "/", "." },
				new String[] { FILE_SEPARATOR, FILE_SEPARATOR });
		logger.info("Java Path: {}", javaPath);
		sqlPath = StringUtil.replaceEach(
				projectPath + FILE_SEPARATOR + ConfigProperties.getOutputDir()
						+ "/sql/" + StringUtil.lowerCase(packageName),
				new String[] { "/", "." }, new String[] { FILE_SEPARATOR,
						FILE_SEPARATOR });
		logger.info("Sql Path: {}", javaPath);
		// 视图文件路径
		viewPath = StringUtil.replace(projectPath + FILE_SEPARATOR
				+ ConfigProperties.getOutputDir()
				+ "/src/main/webapp/WEB-INF/views", "/", FILE_SEPARATOR);
		logger.info("View Path: {}", viewPath);

	}

	public String getJavaPath(String className, String classType) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(javaPath).append(FILE_SEPARATOR).append(moduleName)
				.append(FILE_SEPARATOR);
		// Controller包名为web，其他为classType：entity，dao，service
		if (classType.equalsIgnoreCase(Constant.CONTROLLER)) {
			buffer.append(StringUtil.lowerCase(Constant.WEB));
		} else {
			buffer.append(StringUtil.lowerCase(classType));
		}
		buffer.append(FILE_SEPARATOR)
				.append(StringUtil.lowerCase(subModuleName))
				.append(FILE_SEPARATOR)
				.append(StringUtil.firstToUpper(className));
		// Entity类名不带Entity后缀
		if (!classType.equalsIgnoreCase(Constant.ENTITY)) {
			buffer.append(StringUtil.firstToUpper(classType));
		}
		buffer.append(Constant.JAVA_SURFIX);
		return buffer.toString();
	}

	public String getViewPath(String className, String viewType) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(viewPath).append(FILE_SEPARATOR)
				.append(StringUtil.substringAfterLast(packageName, "."))
				.append(FILE_SEPARATOR).append(moduleName)
				.append(FILE_SEPARATOR)
				.append(StringUtil.lowerCase(subModuleName))
				.append(FILE_SEPARATOR).append(className).append(viewType)
				.append(Constant.JSP_SURFIX);
		return buffer.toString();
	}

	public String getSqlPath(String tableName) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(sqlPath).append(FILE_SEPARATOR).append(moduleName)
				.append(FILE_SEPARATOR).append("table").append(FILE_SEPARATOR)
				.append(StringUtil.lowerCase(subModuleName))
				.append(FILE_SEPARATOR).append(tableName)
				.append(Constant.SQL_SURFiX);
		return buffer.toString();
	}

	public Map<String, String> getModel(String className) {
		Map<String, String> model = MapUtil.newHashMap();
		model.put("packageName", StringUtil.lowerCase(packageName));
		model.put("moduleName", StringUtil.lowerCase(moduleName));
		model.put("subModuleName", StringUtil.isNotBlank(subModuleName) ? "."
				+ StringUtil.lowerCase(subModuleName) : "");
		model.put("classAuthor",
				StringUtil.isNotBlank(classAuthor) ? classAuthor
						: "Auto Generated");
		model.put("classVersion", DateUtil.getDate());
		model.put("functionName", functionName);
		model.put("urlPrefix",
				model.get("moduleName") + StringUtil.lowerCase(subModuleName)
						+ "/" + className);
		model.put("viewPrefix",
				StringUtil.substringAfterLast(model.get("packageName"), ".")
						+ "/" + model.get("urlPrefix"));
		model.put("permissionPrefix",
				model.get("moduleName") + StringUtil.lowerCase(subModuleName)
						+ ":" + className);
		return model;
	}

	@Override
	public String getModuleName() {
		return moduleName;
	}

	@Override
	public String getFunctionName() {
		return functionName;
	}

	@Override
	public String getPackageName() {
		return packageName;
	}

	@Override
	public String getTemplatePath() {
		return tplPath;
	}

	@Override
	public String getExcelPath() {
		return excelPath;
	}

}
