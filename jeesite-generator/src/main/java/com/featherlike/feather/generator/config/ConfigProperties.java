package com.featherlike.feather.generator.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.featherlike.framework.common.util.StringUtil;

/**
 * @author EverWang
 * 
 */
public class ConfigProperties {
	private static String PROP_PATH = System.getProperty("user.dir")
			+ "\\config\\config.properties";
	private static Properties props = new Properties();

	static {
		try {
			props.load(new FileInputStream(new File(PROP_PATH)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getDbtype() {
		return props.getProperty("db.type");
	}

	public static void setDbtype(String dbtype) {
		props.setProperty("db.type", dbtype);
	}

	public static String getExcelPath() {
		return props.getProperty("excel.path");
	}

	public static void setExcelPath(String excelPath) {
		props.setProperty("excel.path", excelPath);
	}

	public static String getOutputDir() {
		return props.getProperty("output.dir");
	}

	public static void setOutputDir(String outputDir) {
		props.setProperty("output.dir", outputDir);
	}

	public static String getTemlateEngine() {
		return props.getProperty("template.engine.type");
	}

	public static void setTemplateEngine(String templateEngine) {
		props.setProperty("tempalate.engine.type", templateEngine);
	}

	public static String getTemplateDir() {
		return props.getProperty("template.dir");
	}

	public static void setTemplateDir(String templateDir) {
		props.setProperty("tempalate.dir", templateDir);
	}

	public static String getPackageName() {
		return props.getProperty("package.name");
	}

	public static void setPackageName(String packageName) {
		props.setProperty("package.name", packageName);
	}

	public static String getModuleName() {
		return props.getProperty("module.name");
	}

	public static void setModuleName(String moduleName) {
		props.setProperty("module.name", moduleName);
	}

	public static String getSubModule() {
		String subModule = props.getProperty("sub.mudule.name");
		if (StringUtil.isEmpty(subModule)) {
			subModule = "";
		}
		return subModule;
	}

	public static void setSubModule(String subModule) {
		props.setProperty("sub.module.name", subModule);
	}

	public static String getFuncName() {
		return props.getProperty("function.name");
	}

	public static void setFuncName(String funcName) {
		props.setProperty("function.name", funcName);
	}

	public static String getAuthorName() {
		return props.getProperty("author.name");
	}

	public static void setAuthorName(String authorName) {
		props.setProperty("author.name", authorName);
	}

	public static String getExcelParser() {
		return props.getProperty("excel.parser");
	}

	public static void setExcelParser(String excelParser) {
		props.setProperty("excel.parser", excelParser);
	}
}
